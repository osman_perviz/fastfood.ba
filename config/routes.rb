Rails.application.routes.draw do

  root :to=>'access#new'
  resources :users
  resources :categories
  resources :items
  resources :access,only: [ :new, :create, :destroy ]
  resources :setup_organization,only: [ :show,:update ]
  resources :admin,only: [:index]

  resources :cart,except: [:new,:show] do
    collection do
      get :clean
    end
  end


  resources :menus  do
    member do
      get :list
    end
  end

  resources :restaurants do
    member do
      get :menage
    end
  end

  resources :daily_menus do
    member do
      get :menage
      put :toggle_visibility
    end
  end

  get 'registration',:to=>'users#new'
  get 'home', :to => 'home#index'

end

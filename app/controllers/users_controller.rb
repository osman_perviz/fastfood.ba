class UsersController < ApplicationController
  #before_action :is_login?,:only => [:new]
  before_action :is_plan_id_present?,:only=>[:new]


  def index
  end

  def new
    @user = User.new
    session[:plan_id] = params[:plan_id]
  end

  def create
    @user = User.create(users_params)
    if @user.save
      session[:user_id] =@user.id
      #session[:plan_id] = nil
      flash[:notice] = "Uspjesno ste kreirali novog korisnika"
      redirect_to after_sign_up_path
    else
      render 'new'
    end
  end

  def update
  end

  def destroy
  end

  def edit
  end

  def show
  end

  private

  def users_params
    params.require(:user).permit(:username,:email,:password,:password_confirmation,:plan_id)
  end

  def after_sign_up_path
    #session[:plan_id] = params[:plan_id]
    setup_organization_path(:organization_setup)
  end

  def is_plan_id_present?
    unless params[:plan_id].present?
      redirect_to home_url,notice: "Niste izabrali vas Plan"
    end
  end
end

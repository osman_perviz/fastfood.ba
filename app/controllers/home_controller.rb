class HomeController < ApplicationController
  skip_before_filter :set_current_user_restaurants
  layout nil
  def index
    @plans = Plan.all.to_a
  end
end
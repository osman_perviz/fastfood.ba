class RestaurantsController < ApplicationController

  layout 'admin_menage_restaurant',:except => :show

  authorize_resource
  skip_authorize_resource :only => :show

  def index
    #if params[:id].present?
    #authorize! :show, @restaurant
      @restaurants = @tenant.restaurants
       #@restaurants =Restaurant.where(:organization_id =>current_user.organizations.find(params[:id]))
    #else
       #@restaurants = Restaurant.all
   # end
  end


  def new
    @restaurant = Restaurant.new
  end


  def create
    @restaurant = Restaurant.new(restaurant_params)
    @restaurant.organization_id = current_user.organizations.first.id
    respond_to do |format|
      if @restaurant.save
        format.html {redirect_to @restaurant,notice: "Uspjesno ste kreirali restoran."}
        format.json {render action:'new',status: :created,location: @restaurant}
      else
        format.html{render action: 'new'}
        format.json{render json:@restaurant.errors,status: :unprocessable_entity}
      end
    end
  end

  def show
    @restaurant = Restaurant.friendly.find(params[:id])
    #check autorization cancan gem
   # authorize! :show, @restaurant
    @menus = Menu.where(:restaurant_id => @restaurant.id)

  end

  def edit
    @restaurant = Restaurant.friendly.find(params[:id])
  end

  def update
    @restaurant = Restaurant.friendly.find(params[:id])
      respond_to do |format|
        if @restaurant.update_attributes(restaurant_params)
          format.html { redirect_to @restaurant, notice: 'Restaurant was successfully updated.' }
          format.json { head :no_content }
        else
          format.html { render action: 'edit' }
          format.json { render json: @restaurant.errors, status: :unprocessable_entity }
        end
      end
  end

  def destroy
    @restaurant = Restaurant.find(params[:id])
    @restaurant.destroy
    respond_to do |format|
      format.html{ redirect_to restaurant_url }
      format.json{ head :no_content }
    end
  end


  def menage
    @restaurant = Restaurant.friendly.find(params[:id])
    @menu = Menu.new
    @daily_menu = DailyMenu.new
  end




  private
  def restaurant_params
    params.require(:restaurant).permit(:name,:description,:organization_id)
  end


end

class MenusController < ApplicationController
  require 'ShoppingCart'


  layout 'admin',:except => :show
  layout 'admin_menage_restaurant',:only=>'list'
  layout "public",:only=>'show'

  def index
   @menus =Menu.where(:restaurant_id => current_user.restaurants.pluck(:id))
  end

  def new
    @category = Category.all
    @menu = Menu.new
  end

  def create
    @menu = Menu.new(menu_params)
    respond_to do |format|
      if @menu.save
        format.html {redirect_to list_menu_url(@menu.id),:notice=>"Uspjesno ste kreirali novi menu"}
        format.json {render action:'show',status: :created,location: @restaurants }
      else
        format.html { render 'menus/new' }
        format.json { render action:@menu.errors,status: :unprocessable_entity }
      end
    end
  end


  def show
    @menu = Menu.find(params[:id])
    @items = @menu.items
    @categories = Category.all
    @menu_category = @menu.categories.pluck(:name).uniq
    cart_items
  end

  def list
    @menu = Menu.find(params[:id])
    @restaurant = Restaurant.find(@menu.restaurant_id)
    @menu_item = @menu.items
    @item = Item.new
    @daily_menu = DailyMenu.new
  end


  private

  def menu_params
    params.require(:menu).permit(:title,:restaurant_id, :items_attributes =>[:id ,:name,:description,:price,:category_id,:_destroy])
  end


end

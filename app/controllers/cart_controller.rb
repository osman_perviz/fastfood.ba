class CartController < ApplicationController

  def index
  end

  def create
    @item = Item.find(params[:item_id])
    if @item
      if params[:quantity].present?
        quantity = params[:quantity].to_i
      else
        quantity = 1
      end

      if quantity > 0
        cart_session.add_items_to_cart(@item.id, quantity)
        @message = "#{ @item.name } added to basket."
        @status = :notice
      else
        @message = "Invalid quantity."
        @status = :error
      end
    else
      @message = "Product not found!"
      @status = :error
    end
    @cart_items = cart_items
    respond_to do |format|
      format.html{ redirect_to :back,:notice=>@message}
      format.js{}
    end
  end

  def destroy
    @item = Item.find(params[:id])
    cart = CartSession.new(session)
    cart.remove_item_from_cart(@item.id)
  end

  def update
  end

  def clean
    session['cart']['items'].clear
  end

  def cart_params
    params.require(:items).permit(:quantity,:item_id)
  end

end

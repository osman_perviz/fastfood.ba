class AccessController < ApplicationController
  layout 'access_registration_layout'
  skip_before_filter :enable_tenant
  skip_before_filter :set_current_user_restaurants
  skip_authorize_resource
  #skip_authorization_check

  def new
  end

  def create
    @user = User.where(:username => params[:username]).first
    if @user && @user.authenticate(params[:password])
      session[:user_id]= @user.id
      flash[:notice]="Uspjesno ste se prijevili"
      redirect_to admin_index_url
    else
      flash[:notice]= "Neispravna kombinacija Username -a i Passworda "
      render 'new'
    end
  end

  def destroy
    session[:user_id] = nil
    flash[:notice]= "Uspjeno ste se odjavili"
    redirect_to new_access_path
  end

end

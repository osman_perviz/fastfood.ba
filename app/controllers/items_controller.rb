class ItemsController < ApplicationController
  layout 'admin_menage_restaurant'
  def new
    @item = Item.new

  end


  def create
    @item = Item.new(item_params)
    respond_to do |format|
      if @item.save
        @current_ajax_item = Item.last
        format.html{redirect_to menage_restaurant_url(@item.daily_menu.restaurant_id)}
        format.js{}
      else
        format.html{redirect_to list_menu_url(@item.menu_id),:notice => 'neradi'}
        format.js{render json:{error:@item.errors.full_messages},status: 422}
      end
    end
  end


  def edit
  @item = Item.find(params[:id])
  end

  def update
    @item = Item.find(params[:id])
    respond_to do |format|
      if @item.update_attributes(item_params)
        format.html{redirect_to root_path,notice:"Uspjesno ste editovali"}
        format.js{}
      else

      end
    end
  end

  def destroy
    @item=Item.find(params[:id])
    respond_to do |format|
      if @item.destroy
        format.html{redirect_to category_path,notice:"Successfully deleted" }
        format.js{}
      end
    end
  end


  private

  def item_params
    params.require(:item).permit(:name,:category_id,:price,:description,:menu_id,:daily_menu_id,:menu_type)
  end

end

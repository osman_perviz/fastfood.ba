class ApplicationController < ActionController::Base

  protect_from_forgery with: :exception

  require 'CartSession'

  #cancan gem before_filter authorize
  #authorize_resource :class=>false

  before_filter :enable_tenant
  before_filter :set_current_user_restaurants

  helper_method :current_user,:cart_session,:cart_items,:cart_total

  def current_user
    User.find_by_id(session[:user_id])
  end

  #shopping cart helpers
  def cart_session
    @cart_session ||= CartSession.new(session)
  end

  def cart_items
    cart_item = ShoppingCart.new(session)
    @cart_items = cart_item.products
  end

  def cart_total
    @sum = 0
    cart_items.each do|item|
      @sum += (item.price * item.quantity)
    end
    @sum
  end

  #cancan gem alert & redirection
  rescue_from CanCan::AccessDenied do |exception|
    redirect_to new_access_url, :alert => exception.message
  end


  private

  def is_login?
    unless session[:user_id]
      flash[:alert]="Morate biti prijavljeni"
      redirect_to new_access_path
      return false
    else
      return true
    end
  end

  def enable_tenant
      @tenant ||= Tenant.new(current_user)
  end

  def set_current_user_restaurants
    @restaurants = Restaurant.where(:organization_id => current_user.organizations)
  end


end

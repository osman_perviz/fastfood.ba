class DailyMenusController < ApplicationController
  layout 'admin_menage_restaurant',:exept => :show

  def index
    @daily_menu= DailyMenu.all
  end

  def new
    @restaurant = Restaurant.find(params[:restaurant_id]) || Restaurant.find(params[:daily_menu][:restaurant_id])
    @daily_menu = DailyMenu.new
    1.times{@daily_menu.items.build}
  end

  def create
    @daily_menu = DailyMenu.new(daily_menu_params)
    if @daily_menu.save
      redirect_to menage_restaurant_path(@daily_menu.restaurant_id)
    else
      render 'daily_menus/new'
    end
  end

  def edit
    @daily_menu = DailyMenu.find(params[:id])
    @restaurant = Restaurant.where(:id => @daily_menu.restaurant_id)
  end

  def update
    @daily_menu = DailyMenu.find(params[:id])
    respond_to do |format|
      if @daily_menu.update(daily_menu_params)
        format.html{redirect_to menage_daily_menu_path(@daily_menu.restaurant_id),notice: 'Created'}
        format.js{}
      end
    end
  end

  def destroy
    @daily_menu = DailyMenu.find(params[:id])
    respond_to do |format|
      if @daily_menu.delete
        @daily_menu.items.delete_all
        format.html{redirect_to menage_restaurant_path(@daily_menu),notice:"Successfully deleted" }
        format.js{}
      else
        flash[:notice]="neide "
      end
    end
  end

  def menage
    @daily_menu = DailyMenu.where(:restaurant_id => params[:id])
    @restaurant = Restaurant.find(params[:id])

  end

  def toggle_visibility
    @daily_menu = DailyMenu.find(params[:id])
    @daily_menu.toggle!(:visibility)
  end


  private

  def daily_menu_params
    params.require(:daily_menu).permit(:title,:restaurant_id,:price,:visibility,items_attributes:[:id,:name,:category_id,:description,:menu_type,:daily_menu_id,:_destroy])
  end




end

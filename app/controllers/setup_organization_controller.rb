class SetupOrganizationController < ApplicationController

  include Wicked::Wizard

  steps :organization_setup

  def show
    @user = current_user
    @organization = Organization.new
    case steps
      when :organization_setup
        @organization = Organization.new
    end
    render_wizard
  end

  def update
    @user = current_user
    @organization = Organization.new(organization_params)
    @organization.users << @user
    @user.add_role "owner"
    render_wizard @organization
  end

  private

  def finish_wizard_path
    new_access_url
  end

  def organization_params
    params.require(:organization).permit(:name,:description,:plan_id,{:user_ids=>[]})
  end
end

class User < ActiveRecord::Base
  rolify
  has_secure_password
  has_one :plan
  has_many :restaurants,:through => :organizations

  has_and_belongs_to_many :organizations

  validates :username,
            :presence => true,
            :uniqueness => true,
            :length => { maximum: 30 }

  validates :email,
            presence: true,
            format: { with: /\A([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})\Z/i, on: :create }



end

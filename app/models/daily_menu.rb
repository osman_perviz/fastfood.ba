class DailyMenu < ActiveRecord::Base

  has_many :items,dependent: :destroy
  belongs_to :restaurant,:dependent => :destroy

  accepts_nested_attributes_for :items,
                                :allow_destroy => true,
                                :reject_if => :all_blank

  validates :title,
            :presence => true

  validates :restaurant_id,
            :presence=> true

  validates :price,
            :presence => true,
            :numericality => true
end

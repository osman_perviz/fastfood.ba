class Restaurant < ActiveRecord::Base
  extend FriendlyId
  friendly_id :name, use: :slugged
  has_one :menu,dependent: :destroy
  has_many :daily_menus,dependent: :destroy

  belongs_to :organization
  validates :name,
            presence: true
end

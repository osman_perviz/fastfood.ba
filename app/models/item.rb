class Item < ActiveRecord::Base

  belongs_to :menu
  belongs_to :daily_menu
  belongs_to :category

  validates :name,
            :presence => true
           # :length => {minimum: 2,maximum: 45}
 # validates :price,
           # :presence => true,
           # :numericality => true
  validates :category_id,
            :presence => true

end

class Category < ActiveRecord::Base

  has_many :items
   has_many :menus
  validates :name,
            :presence => true
end

class Tenant

  def initialize(user)
    @user = user
  end

  def restaurants
    owner? ?  Restaurant.where('organization_id = ?',@user.organizations.first.id).all : Restaurant.all.all
  end

  private



  def admin?
    @user.has_role? "admin"
  end

  def owner?
    @user.has_role? "owner" if @user
  end
end
class Ability
  include CanCan::Ability

  def initialize(user)
    user ||= User.new #not logged in user
      if user.has_role? :admin
        can :manage, :all
      elsif user.has_role? :owner
        can :manage,Restaurant,:organization_id => user.organizations.first.id
        can :manage,Menu
        can :manage,:admin
        can :read, :home

      else
        can :read, :home
      end
  end
end

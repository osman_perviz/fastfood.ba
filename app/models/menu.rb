class Menu < ActiveRecord::Base
  belongs_to :restaurant
  has_many :categories,:through => :items
  has_many :items,:dependent => :destroy

  validates :title,
            :presence => true


end

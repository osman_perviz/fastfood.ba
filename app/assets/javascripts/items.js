$(document).ready(function(){
    $('.new_item').on('ajax:error',function(e,xhr,state, error){
        var errorJson = JSON.parse(xhr.responseText).error;
        $(".errors_div").empty()
        $.each(errorJson,function (key,value) {
            $(".errors_div").append("<li>"+ errorJson[key]+ "</li>")
        });

    })

    $('a[href*=#]:not([href=#])').click(function(){
        var target = this.hash
        var $target = $(this.hash).offset().top
        $('html,body').stop().animate({
            scrollTop : $target
        },1500,function(){
            window.location = target;
        });
    });

});
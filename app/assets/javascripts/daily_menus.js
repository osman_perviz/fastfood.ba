

/* sa objektom neradi(nelogicno),radi sa arrray-om,popraviti poslje */
function append_value_and_text_to_select_tag(x){
    var obj = $('#daily_menu_items_attributes_0_category_id option ')
    var optionValues = [];
    var optionText = [];

    obj.each(function(){
        optionText.push($(this).text());
    });
    obj.each(function(){
        optionValues.push($(this).val());
    });
    var form =  $('#daily_menu_items_attributes_'+ x +'_category_id');
    $.each(optionText,function(index,value){
        form.append("<option value='" + optionValues[index]  + "'>" + optionText[index] + "</option>")
    })


}

/* adding nested item custum function*/
function append_nested_form_to_daily_menu_form(x){
    var max_fields = 4;
    event.preventDefault();
    if (x > max_fields) {
        alert('Broj jela je ogranicen na 3');
        $('.newItem').fadeOut()

    } else {
        ++x;
        $("#itemsWrapper").append("<div class='form-group col-lg-12 new_item'>" +
            "<div class='col-lg-4'>" +
            "<label class ='label_name'>Name</label><br>" + "<input class='form-control' id = 'daily_menu_items_attributes_" + x + "_name'  name='daily_menu[items_attributes][" + x + "][name]' placeholder='Name' type='text'>"+ "</div>" +
            "<input  id='daily_menu_items_attributes_" + x + "_menu_type' name='daily_menu[items_attributes][" + x + "][menu_type]' type='hidden' value='false'>" +
            "<div class='col-lg-4'>" +
            "<label>Category</label><br>" +
            "<select class='form-control category_option' id='daily_menu_items_attributes_" + x + "_category_id' name='daily_menu[items_attributes][" + x + "][category_id]'>" + "</select>" + "</div>" +
            "<div class='col-lg-4'>" + "<label>Description</label><br>" +
            "<input class='form-control' id='daily_menu_items_attributes_" + x + "_description' name='daily_menu[items_attributes][" + x + "][description]' placeholder='Short Description'>"  + "</div>" +
            "<a href='#' class='btn btn-primary btn-sm remove_field pull-right '> Remove Meal" + "</a>"

        );
        append_value_and_text_to_select_tag(x)
    }

}


$(document).ready(function(){
    var add_button = $(".newItem");

    $(add_button).click(function(){
        var x = $('.label_name').length + 1;
        append_nested_form_to_daily_menu_form(x);
    });

    $("#itemsWrapper").on("click",".remove_field", function(){
        event.preventDefault();
        $(this).parent('div').remove();
        x--;
    });




});

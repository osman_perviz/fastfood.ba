module ApplicationHelper

  def error_messages_for(object)
    #render error message(views/application html template),i prilagodjavanje lokalnih varijabli unutar 'application/error_message sa ulaznom
    render(:partial => 'application/error_messages', :locals => {:object => object})
  end

  def plan_name(plan_id)
    plan_name = Plan.where(:id => plan_id).pluck(:name).first
  end

  def active_link?(test_path)
    return 'active' if request.original_url == test_path
    ''
  end

  def set_visibility_text(obj)
    if obj.visibility
      "Visible"
    else
      "Hiden"
    end
  end

  def set_class_for_visibility(obj)
    if obj.visibility
      "btn btn-danger btn-md visibility"
    else
      "btn btn-primary btn-md visibility"
    end
  end



end

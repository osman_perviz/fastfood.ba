class AddCategoryIdToItems < ActiveRecord::Migration
  def up
    add_column :items,:category_id,:integer
  end

  def down
    remove_column :items,:category_id
  end
end

class AddOrganizationIdToRestaurants < ActiveRecord::Migration
  def up
    add_column :restaurants, :organization_id, :integer
  end

  def down
    remove_column :restaurants,:organization_id
  end
end

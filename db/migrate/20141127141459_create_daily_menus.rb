class CreateDailyMenus < ActiveRecord::Migration
  def change
    create_table :daily_menus do |t|
      t.string :title
      t.integer :restaurant_id
      t.integer :price
      t.boolean :visibility, default:true,null: false

      t.timestamps
    end
  end
end

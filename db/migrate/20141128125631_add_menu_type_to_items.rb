class AddMenuTypeToItems < ActiveRecord::Migration
  def change
    add_column :items, :menu_type, :boolean,null:false, default: true
  end
end

class AddMenuIdToItems < ActiveRecord::Migration
  def up
    add_column :items ,:menu_id ,:integer
  end

  def down
    remove_column :items ,:menu_id
  end
end

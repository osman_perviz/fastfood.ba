class AddRestaurantIdToMenus < ActiveRecord::Migration
  def up
    add_column :menus ,:restaurant_id ,:integer
  end

  def down
    remove_column :menus ,:restaurant_id
  end
end

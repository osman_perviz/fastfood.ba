class AlterColumnItemsPrice < ActiveRecord::Migration
  def up
    change_table :items do |t|
      t.change :price,:decimal,:precision => 10, :scale => 2
    end
  end
  def down
    change_table :items do |t|
      t.change :price, :integer
    end
  end
end

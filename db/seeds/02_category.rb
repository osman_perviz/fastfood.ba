
## seed Category
category = [
    "Hors d'oeuvres",
    "Soups & Stews",
    "Hot Appetizers",
    "Cooked Dishes",
    "Grill & Steaks",
    "Chicken and Turkey",
    "La carte dishes",
    "Fish and Mollusks",
    "Crabs & Shells",
    "Specialty of the House",
    "Pizza",
    "Condiments",
    "Additions to dishes",
    "Salads"
]
category.each do |cat|
  Category.create(:name => cat)
end

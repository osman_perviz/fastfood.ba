## seed Plans
plans = [
    ["Small",1,10,20,5,10],
    ["Meedium",5,50,50,10,30],
    ["Large",10,100,50,50,50]
]
plans.each do |name, restaurants, tables, menu_items, storage, price |
  Plan.create( name:name,restaurants:restaurants,tables:tables,menu_items:menu_items,storage:storage,price:price)
end


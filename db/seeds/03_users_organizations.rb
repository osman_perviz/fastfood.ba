## seed Users
users = [
    ["user1","user1","user1@gmail.com",1],
    ["user2","user2","user2@gmail.com",2],
    ["user3","user3","user3@gmail.com",3]
]
users.each do |username,pass,email,plan_id|
  User.create!({username:username,password:pass,email:email,plan_id:plan_id})
end
user1= User.where(username: 'user1').first
user2= User.where(username: 'user2').first
user3= User.where(username: 'user3').first

##seed organizations
organizations = [
    ["organization_user2","description_user1 bla bla balaaaaa"],
    ["organization_user3","description_user2 bla bla balaaaaa"]
]
organizations.each do |name,desc|
  Organization.create(name:name,description:desc)
end

org2 = Organization.where(:name => "organization_user2").first
org3 = Organization.where(:name => "organization_user3").first


##add roles and organization too user
user1.add_role :admin

user2.add_role :owner

user3.add_role :owner

user2.organizations << org2
user3.organizations << org3


Dir[File.join(Rails.root, 'db', 'seeds', '*.rb')].sort.each { |seed| load seed }

## seed menus
1.upto(7) do |menu|
  Menu.create(:title=> "Omega_#{menu} menu",:description=>"Some description for menu omega_#{menu}",:restaurant_id=> menu)
end

##seed daily menus
1.upto(21) do |daily|
  DailyMenu.create(:title=>"Daily #{daily}_title",:restaurant_id=>rand(0..10),
                   :price=>(daily * 3),:visibility => true)
end



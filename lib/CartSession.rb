class CartSession

  def initialize(session)
    @session = session

    #set the shopping cart
    @session['cart'] ||= {}

    #set cart items
    @session['cart']['items'] ||= {}
   # @session[:expires_at] = Time.now


  end

  def cart_hash
    @session['cart']
  end

  def cart_products_hash
    @session['cart']['items']
  end

  def add_items_to_cart(item_id,quantity)
    quantity ||= 1
    item_id = item_id.to_s
    current_quantity = cart_products_hash.fetch(item_id,{}).fetch('quantity',0)
    quantity += current_quantity
    cart_products_hash[item_id] = {'quantity' => quantity}
  end

  def remove_item_from_cart(item_id)
    item_id = item_id.to_s
    cart_products_hash.delete(item_id)
  end

end
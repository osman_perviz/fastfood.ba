
class ShoppingCart
  require 'ShoppingCartItem'

  def initialize(session)
    @session = session
    @products = get_products_from_session
  end

  def get_products_from_session
    products = cart_product_hash

    items = []

    products.keys.each do |item_id|
      product = Item.find(item_id.to_i)

      if product
        item = ShoppingCartItem.new(product, products[item_id]['quantity'])
        items << item if item
      else
        # If the product was not found in the database -> remove it from the cart
        @session.remove_item_from_cart(item_id)
      end
    end

    items
  end

  def products
    @products
  end


  def cart_product_hash
    @session['cart']['items']
  end



end